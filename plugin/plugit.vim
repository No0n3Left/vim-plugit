" plugin/plugit.vim
scriptencoding utf-8

" Only load script if needed
if (exists("g:loaded_plugit") && g:loaded_plugit) || &cp
	finish
endif
let g:loaded_plugit = 1

" Set configuration defaults
function! s:set(var, default, ...) abort
	if !exists(a:var)
		for message in a:000
			echohl WarningMsg
			echo 'vim-plugit: ' . message
			echohl None
			let v:warningmsg = message
		endfor
		if type(a:default)
			execute 'let' a:var '=' string(a:default)
		else
			execute 'let' a:var '=' a:default
		endif
	endif
endfunction
call s:set('g:plugit_directory', expand(get(runtimepath, 0, '~/.vim/') . 'plug' . $USER))
call s:set('g:plugit_submodule_enable', 0, 'g:plugit_submodule_enable not set, defaulting to 0. g:plugit_submodule_enable should be explicitly set.')

function! s:PlugitComplete(A, L, P)
	if !exists(g:plugit_plugins)
		call plugit#refresh()
	endif
	return keys(g:plugit_plugins)
endfunction

" Commands
command! -bar -nargs=+ PlugitInstall call plugit#install(<f-args>)
command! -bar PlugitUpdateAll call plugit#update()
command! -bar -nargs=+ -complete=custom,s:PlugitComplete PlugitUpdate call plugit#update()
command! -bar -bang -nargs=+ -complete=custom,s:PlugitComplete PlugitRemove call plugit#remove(<f-args>)
command! -bar -bang -nargs=+ -complete=custom,s:PlugitComplete PlugitDelete call plugit#remove(<f-args>)
command! -bar -bang -nargs=+ -complete=custom,s:PlugitComplete PlugitUninstall call plugit#remove(<f-args>)
command! -bar PlugitRefresh call plugit#refresh()

function! PlugitUpdate()
	system('git --git-dir=' . g:plugit_directory . ' submodule update --remote --merge')
endfunc
