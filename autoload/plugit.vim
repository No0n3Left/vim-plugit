" autoload/plugit.vim

" Run :helptags on all doc directories in g:plugit_plugins
function! plugit#helptags() abort
	call plugit#
endfunction

function! plugit#subsys#plugits() abort
	let gitmodules = system('cat $(git rev-parse --show-toplevel)/gitmodules')
	let submodules = []
	for line in gitmodules
		if line ~= '\v^\s*path = \s*(.*)'
			let submodules = add(submodules, line)
		endif
	endfor
	perl -ne '/^\s*path =\s*(.*)/ and push(@submods, $1); END { print(join("\n", sort(@submods)));}' "$(git rev-parse --show-toplevel)/.gitmodules"
endfunction

function! plugit#warn(message) abort
	echohl WarningMsg
	echo 'vim-plugit: ' . a:message
	echohl None
	let v:warningmsg = a:message
endfunction

function! plugit#refresh() abort
	let g:plugit_plugins = {}
	if g:plugit_submodule_enable
		let g:plugit_submodule_gitdir = system("git --git-dir=" . g:plugit_directory . "rev-parse --show-toplevel")
		let submodules = system("git --git-dir=" . g:plugit_submodule_gitdir . "submodule foreach 'echo $path `git config --get remote.origin.url`'")
		for submodule in submodules
			call extend(g:plugit_plugins, { submodule[0]: [ submodule[1], submodule[2] ], })
		endfor
		echom g:plugit_plugins
	else
		" TODO
		plugit#warn("Not yet implemented.")
	endif
endfunction

function! plugit#install() abort
	if !exists("g:plugit_plugins")
		plugit#refresh()
	endif
	
endfunction
